package com.example.android.marsrealestate.vmc

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VmcProperty(
        val id: String,
        val model: String,
        val x: String,
        @Json(name = "axiscount") val axes: String,
        @Json(name = "video1") val img: String) : Parcelable
