package com.example.android.marsrealestate.lathe.oneLathe

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android.marsrealestate.lathe.Lathe

class LatheOneViewModel(lathe: Lathe, app: Application) : AndroidViewModel(app) {

    val selectedProperty: LiveData<Lathe>  get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<Lathe>()


    init {
        _selectedProperty.value = lathe
    }


}